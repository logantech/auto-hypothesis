# Hypothesis testing

* Starting with a [UCI dataset](https://archive.ics.uci.edu/ml/datasets/Automobile) describing pricing and features of one model year of automobiles, explore data to find trends, created hypotheses and make predictions.

Quickstart:

* Download Python 3 notebook: LoganDowning-M02-HypothesisSim.ipynb
* Download datasource: Automobile.csv
* Open using Jupyter Notebook

Or view these [screen grabs](screenshots/) of the entire notebook.
___

Create QQ plots to test features against normality.

![QQ plot of a feature](images/qq.png)

Perform KS-tests to quantify feature distributions.

![KS plot of a feature](images/ks.png)

Use bootstrapping to calculate summary statistics.

![Histogram of bootstrapped means](images/bootstrap.png)

Use classical statistical methods to evaluate differences in distributions for two features.

![Two histograms showing difference in distributions between two features](images/hist.png)

Use Bayesian techniques to improve the estimate the mean and standard deviation of a feature using holdout information.

![Bayes plot showing priors, likelihoods and posteriors](images/bayes.png)



Use bootstrapping to detect differences in populations. Calculated the differences in means of bootstrapped samples.

![histogram showing bootstrapped means](images/bootstrap-means.png)



Evaluate the significance of multiple categorical features using Tukey’s HSD.

![Graph of Tukey's HSD results showing the significance of mutiple features](images/tukey.png)
